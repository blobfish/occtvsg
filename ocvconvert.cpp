/* occtvsg. Connecting OpenCasCade and VulkanSceneGraph.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <optional>
#include <cassert>

#include <TopoDS_Iterator.hxx>
#include <TopoDS.hxx>
#include <TopExp.hxx>
#include <BRep_Tool.hxx>
#include <Poly_Triangulation.hxx>
#include <Poly_Polygon3D.hxx>
#include <Poly_PolygonOnTriangulation.hxx>
#include <GeomLib.hxx>

#include "ocvconvert.hpp"

using namespace ocv;



bool ShapeConvert::generate()
{
  faceGroup = vsg::Group::create();
  edgeGroup = vsg::Group::create();
  TopExp::MapShapesAndAncestors(shape, TopAbs_EDGE, TopAbs_FACE, edgeToFace);
  return recurse(shape);
}

bool ShapeConvert::recurse(const TopoDS_Shape &rShape)
{
  for (TopoDS_Iterator it(rShape); it.More(); it.Next())
  {
    const TopoDS_Shape &currentShape = it.Value();
    if (processed.Contains(currentShape)) continue;
    processed.Add(currentShape);
    TopAbs_ShapeEnum currentType = currentShape.ShapeType();
    if
    (
      (currentType == TopAbs_COMPOUND) ||
      (currentType == TopAbs_COMPSOLID) ||
      (currentType == TopAbs_SOLID) ||
      (currentType == TopAbs_SHELL) ||
      (currentType == TopAbs_WIRE)
    )
    {
      if (!recurse(currentShape)) return false;
      continue;
    }
    try
    {
      if (currentType == TopAbs_FACE)
      {
        FaceConvert fConvert(TopoDS::Face(currentShape));
        bool result = fConvert.generate();
        if (!result)
        {
          errors = fConvert.getErrors();
          return false;
        }
        warnings.insert(warnings.end(), fConvert.getWarnings().begin(), fConvert.getWarnings().end());
        faceGroup->addChild(fConvert.buildDraw());
        if (!recurse(currentShape)) return false;
        continue;
      }
      if (currentType == TopAbs_EDGE)
      {
        TopoDS_Face parentFace;
        if (edgeToFace.Contains(currentShape))
        {
          const auto &list= edgeToFace.FindFromKey(currentShape);
          if (!list.IsEmpty()) parentFace = TopoDS::Face(list.First());
        }
        EdgeConvert eConvert(TopoDS::Edge(currentShape), parentFace);
        bool result = eConvert.generate();
        if (!result)
        {
          errors = eConvert.getErrors();
          return false;
        }
        warnings.insert(warnings.end(), eConvert.getWarnings().begin(), eConvert.getWarnings().end());
        edgeGroup->addChild(eConvert.buildDraw());
        if (!recurse(currentShape)) return false;
        continue;
      }
    }
    catch(const Standard_Failure &error)
    {
      errors.emplace_back("occt exception caught: ");
      errors.back() += error.GetMessageString();
      return false;
    }
    catch(const std::exception &error)
    {
      errors.emplace_back("std exception caught: ");
      errors.back() += error.what();
      return false;
    }
    catch(...)
    {
      errors.emplace_back("unknown exception caught");
      return false;
    }
  }
  return true;
}


bool FaceConvert::generate()
{
  assert(!face.IsNull());
  TopLoc_Location location;
  const auto &triangulation = BRep_Tool::Triangulation(face, location);
  //did a test and triangulation doesn't have normals.
  if (triangulation.IsNull())
  {
    errors.push_back("null triangulation in face construction");
    return false;
  }
  
  vertices = vsg::vec3Array::create(triangulation->NbNodes());
  normals = vsg::vec3Array::create(triangulation->NbNodes());
  coordinates = vsg::vec2Array::create(triangulation->NbNodes());
  indexes = vsg::uintArray::create(triangulation->NbTriangles() * 3);
  
  bool isReversed = face.Orientation() == TopAbs_REVERSED;
  
  std::optional<gp_Trsf> transformation;
  if(!location.IsIdentity()) transformation = location.Transformation();
  
  //vertices.
  for (int index = 1; index < triangulation->NbNodes() + 1; ++index)
  {
    gp_Pnt point = triangulation->Node(index);
    if(transformation) point.Transform(*transformation);
    vertices->at(index - 1) = vsg::vec3(point.X(), point.Y(), point.Z());
    
    //maybe more here for normalizing to square texture.
    gp_Pnt2d uvPoint = triangulation->UVNode(index);
    coordinates->at(index - 1) = vsg::vec2(uvPoint.X(), uvPoint.Y());
  }
  
  //normals.
  if (!triangulation->HasUVNodes())
  {
    errors.push_back("No UV Nodes in triangulation");
    return false;
  }
  opencascade::handle<Geom_Surface> surface = BRep_Tool::Surface(face);
  if (surface.IsNull())
  {
    errors.push_back("null surface");
    return false;
  }
  for (int index = 1; index < triangulation->NbNodes() + 1; ++index)
  {
    gp_Dir direction; //defaults to xaxis.
    int result = GeomLib::NormEstim(surface, triangulation->UVNode(index), Precision::Confusion(), direction);
    if (result != 0) warnings.push_back("GeomLib::NormEstim failed");
    if (isReversed) direction.Reverse();
    normals->at(index - 1) = vsg::vec3(direction.X(), direction.Y(), direction.Z());
  }
  
  for (int index = 1; index < triangulation->NbTriangles() + 1; ++index)
  {
    int N1, N2, N3;
    triangulation->Triangle(index).Get(N1, N2, N3);
    
    int factor = (index - 1) * 3;
    indexes->at(factor) = N1 - 1;
    indexes->at(factor + 1) = N2 - 1;
    indexes->at(factor + 2) = N3 - 1;
    if (isReversed) std::swap(indexes->at(factor), indexes->at(factor + 2));
  }
  
  return true;
}

vsg::ref_ptr<vsg::VertexIndexDraw> FaceConvert::buildDraw()
{
  vsg::DataList arrays;
  arrays.push_back(vertices);
  arrays.push_back(normals);
  arrays.push_back(coordinates);
  arrays.push_back(vsg::vec4Array::create(vertices->size(), vsg::vec4(1.0, 0.0, 0.0, 1.0)));
  arrays.push_back(vsg::vec3Array::create(1)); //positions
  
  auto vid = vsg::VertexIndexDraw::create();
  vid->assignArrays(arrays);
  vid->assignIndices(indexes);
  vid->indexCount = indexes->size();
  vid->instanceCount = 1;
  
  return vid;
}

bool ocv::EdgeConvert::generate()
{
  assert(!edge.IsNull());
  
  if (face.IsNull())
  {
    TopLoc_Location location;
    const auto& poly = BRep_Tool::Polygon3D(edge, location);
    if (poly.IsNull())
    {
      errors.push_back("Poly on faceless edge is null");
      return false;
    }
    
    std::optional<gp_Trsf> transformation;
    if(!location.IsIdentity()) transformation = location.Transformation();
    
    const TColgp_Array1OfPnt& nodes = poly->Nodes();
    vertices = vsg::vec3Array::create(nodes.Size());
    unsigned int tempIndex = 0;
    for (auto point : nodes) //can't const ref, might transform.
    {
      if (!transformation) point.Transform(*transformation);
      vertices->at(tempIndex) = vsg::vec3(point.X(), point.Y(), point.Z());
      tempIndex++;
    }
  }
  else //edge on face
  {
    TopLoc_Location location;
    Handle(Poly_Triangulation) triangulation;
    triangulation = BRep_Tool::Triangulation(face, location);
    
    const auto &segments = BRep_Tool::PolygonOnTriangulation(edge, triangulation, location);
    if (segments.IsNull())
    {
      errors.push_back("edge triangulation is null in edge construction with face");
      return false;
    }
    
    std::optional<gp_Trsf> transformation;
    if(!location.IsIdentity()) transformation = location.Transformation();
    
    const TColStd_Array1OfInteger& indexes = segments->Nodes();
    vertices = vsg::vec3Array::create(indexes.Length());
    unsigned int tempIndex = 0;
    for (int index(indexes.Lower()); index < indexes.Upper() + 1; ++index, ++tempIndex)
    {
      gp_Pnt point = triangulation->Node(indexes(index));
      if(transformation) point.Transform(*transformation);
      vertices->at(tempIndex) = vsg::vec3(point.X(), point.Y(), point.Z());
    }
  }
  
  return true;
}

vsg::ref_ptr<vsg::VertexDraw> EdgeConvert::buildDraw()
{
  vsg::DataList arrays;
  arrays.push_back(vertices);
  arrays.push_back(vsg::vec3Array::create(vertices->size(), vsg::vec3(0.0, 0.0, 1.0)));
  // arrays.push_back(coordinates);
  arrays.push_back(vsg::vec4Array::create(vertices->size(), vsg::vec4(0.0, 0.0, 0.0, 1.0)));
  arrays.push_back(vsg::vec3Array::create(1)); //positions
  
  auto vid = vsg::VertexDraw::create();
  vid->assignArrays(arrays);
  vid->vertexCount = vertices->width();
  // vid->assignIndices(indexes);
  // vid->indexCount = indexes->size();
  vid->instanceCount = 1;
  
  return vid;
}
