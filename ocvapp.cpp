/* occtvsg. Connecting OpenCasCade and VulkanSceneGraph.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cassert>

#include <cxxopts.hpp>

#include <BRepTools.hxx>
#include <BRep_Builder.hxx>

#include <vsg/io/write.h>
#include <vsg/nodes/Group.h>
#include <vsg/nodes/VertexIndexDraw.h>
#include <vsg/nodes/StateGroup.h>
#include <vsg/state/RasterizationState.h>
#include <vsg/state/InputAssemblyState.h>
#include <vsg/utils/GraphicsPipelineConfigurator.h>
#include <vsg/utils/Builder.h>

#include "ocvconvert.hpp"

/* How to pass mesh parameters?
 * What IPC? just read and write to directories.
 * 
 * example of launching generator.
 * vsgOCC generator inputpath outputpath
 * failure if inputPath and outputPath don't exist
 * failure if inputPath or outputPath are not empty
 * 
 * example of launching single operation
 * vsgOCC file0.brep file1.brep
 * outputs will have same name with .vsg extension
 * 
 * 
 */

vsg::ref_ptr<vsg::VertexIndexDraw> buildTriangle()
{
  auto vertices = vsg::vec3Array::create
  (
    {
      {0.0, 0.0, 0.0}
      , {10.0, 0.0, 0.0}
      , {5.0, 8.66, 0.0}
    }
  );
  
  auto normals = vsg::vec3Array::create(3, vsg::vec3(0.0, 0.0, 1.0));
  //   ref_ptr<vec2Array> texcoords;
  // auto colors = vsg::vec4Array::create(3, vsg::vec4(1.0, 0.0, 0.0, 1.0));
  auto indices = vsg::ushortArray::create({0, 1, 2});
  
  vsg::DataList arrays;
  arrays.push_back(vertices);
  arrays.push_back(normals);
  // arrays.push_back(vsg::vec2Array::create(3)); //texture coordinates
  // arrays.push_back(colors);
  // arrays.push_back(vsg::vec3Array::create(1)); //positions
  
  auto vid = vsg::VertexIndexDraw::create();
  vid->assignArrays(arrays);
  vid->assignIndices(indices);
  vid->indexCount = indices->size();
  vid->instanceCount = 1;
  
  return vid;
}

std::string vertexCode{R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(push_constant) uniform PushConstants {mat4 projection; mat4 modelView;};
layout(location = 0) in vec3 vertex0;
layout(location = 1) in vec3 normal0;
layout(location = 0) out vec3 normal1;
out gl_PerVertex {vec4 gl_Position;};
void main()
{
  gl_Position = (projection * modelView) * vec4(vertex0, 1.0);
  normal1 = (modelView * vec4(normal0, 1.0)).xyz;
}
)"};

std::string fragmentCode{R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(set = 0, binding = 0) uniform ColorData {vec4 theColor;} color0;
layout(location = 0) in vec3 normal1;
layout(location = 0) out vec4 color;
void main() {color = color0.theColor;}
)"};

int main(int argc, char **argv)
{
  //vsg::CommandLine and vsg::Options don't appear to be to vsg specific.
  cxxopts::Options options("vsgOCC", "Vulkan Scene Graph generation from OpenCasCade");
  options.add_options()
  ("s,service", "Service")
  ("i,input", "Input Directory", cxxopts::value<std::string>())
  ("o,output", "Output Directory", cxxopts::value<std::string>())
  ("f,file", "OCCT file", cxxopts::value<std::string>())
  ;
  
  auto result = options.parse(argc, argv);
  if (result.count("service") != 0)
    std::cout << "Service" << std::endl;
  else
    std::cout << "No service" << std::endl;
  if (result.count("input") != 0)
    std::cout << "Input Directory is: " << result["input"].as<std::string>() << std::endl;
  else
    std::cout << "No Input Directory" << std::endl;
  if (result.count("output") != 0)
    std::cout << "Output Directory is: " << result["output"].as<std::string>() << std::endl;
  else
    std::cout << "No Output Directory" << std::endl;
  
  auto vsgOptions = vsg::Options::create();
  vsgOptions->paths = vsg::getEnvPaths("VSG_FILE_PATH");
  
  auto scene = vsg::Group::create();
  auto fragColor = vsg::vec4Array::create(1, vsg::vec4(1.0, 1.0, 0.0, 1.0));
  
  auto vertexShader = vsg::ShaderStage::create(VK_SHADER_STAGE_VERTEX_BIT, "main", vertexCode);
  auto fragmentShader = vsg::ShaderStage::create(VK_SHADER_STAGE_FRAGMENT_BIT, "main", fragmentCode);
  auto shaderSet = vsg::ShaderSet::create(vsg::ShaderStages{vertexShader, fragmentShader});
  shaderSet->addPushConstantRange("pc", "", VK_SHADER_STAGE_VERTEX_BIT, 0, 128);
  shaderSet->addAttributeBinding("vertex0", "", 0, VK_FORMAT_R32G32B32_SFLOAT, vsg::vec3Array::create(1));
  shaderSet->addAttributeBinding("normal0", "", 1, VK_FORMAT_R32G32B32_SFLOAT, vsg::vec3Array::create(1));
  shaderSet->addDescriptorBinding("color0", "", 0, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, fragColor);
  auto pipeConfig = vsg::GraphicsPipelineConfigurator::create(shaderSet);
  pipeConfig->enableArray("vertex0", VK_VERTEX_INPUT_RATE_VERTEX, 12);
  pipeConfig->enableArray("normal0", VK_VERTEX_INPUT_RATE_VERTEX, 12);
  pipeConfig->enableDescriptor("color0");
  pipeConfig->init();
  auto stateGroup = vsg::StateGroup::create();
  pipeConfig->copyTo(stateGroup);
  scene->addChild(stateGroup);
  
  if (result.count("file") == 0)
  {
    stateGroup->addChild(buildTriangle());
    assert(vsg::write(scene, "triangle.vsgt"));
  }
  else
  {
    BRep_Builder wtf;
    TopoDS_Shape shapeIn;
    if (!BRepTools::Read(shapeIn, result["file"].as<std::string>().c_str(), wtf))
    {
      std::cerr << "Couldn't read occt file" << std::endl;
      return 1;
    }
    ocv::ShapeConvert c(shapeIn);
    if (!c.generate())
    {
      const auto &es = c.getErrors();
      for (const auto &e : es) std::cerr << e << std::endl;
      return 1;
    }
    stateGroup->addChild(c.getFaces());
    assert(vsg::write(scene, "faces.vsgt"));
  }
  return 0;
}
