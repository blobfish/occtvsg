/* occtvsg. Connecting OpenCasCade and VulkanSceneGraph.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCV_CONVERT_H
#define OCV_CONVERT_H

#include <vector>
#include <string>

#include <vsg/core/Array.h>
#include <vsg/nodes/Group.h>
#include <vsg/nodes/VertexIndexDraw.h>
#include <vsg/nodes/VertexDraw.h>

#include <TopoDS_Shape.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Edge.hxx>
#include <TopTools_MapOfShape.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>

namespace ocv
{
  struct BaseConvert
  {
    const std::vector<std::string>& getWarnings(){return warnings;}
    const std::vector<std::string>& getErrors(){return errors;}
  protected:
    BaseConvert() = default;
    std::vector<std::string> warnings;
    std::vector<std::string> errors;
  };
  
  /*! @struct ShapeConvert
   * @brief generate VSG representation from OCCT Shape.
   * @details generate VSG representation from OCCT Shape.
   * Assumes the shape has been infused with the desired mesh
   * prior to being passed into constructor.
   */
  struct ShapeConvert : public BaseConvert
  {
    ShapeConvert() = delete;
    explicit ShapeConvert(const TopoDS_Shape &sIn) : BaseConvert(), shape(sIn){}
    bool generate(); //return false when errors. warnings are still true;
    vsg::ref_ptr<vsg::Group> getFaces(){return faceGroup;}
    vsg::ref_ptr<vsg::Group> getEdges(){return edgeGroup;}
  private:
    TopoDS_Shape shape;
    TopTools_MapOfShape processed;
    TopTools_IndexedDataMapOfShapeListOfShape edgeToFace;
    vsg::ref_ptr<vsg::Group> faceGroup;
    vsg::ref_ptr<vsg::Group> edgeGroup;
    bool recurse(const TopoDS_Shape&);
  };
  
  struct FaceConvert : public BaseConvert
  {
    FaceConvert() = delete;
    explicit FaceConvert(const TopoDS_Face &fIn) : BaseConvert(), face(fIn){}
    
    bool generate(); //return false when errors. warnings are still true;
    vsg::ref_ptr<vsg::VertexIndexDraw> buildDraw();
    vsg::ref_ptr<vsg::vec3Array> getVertices(){return vertices;}
    vsg::ref_ptr<vsg::vec3Array> getNormals(){return normals;}
    vsg::ref_ptr<vsg::vec2Array> getCoordinates(){return coordinates;}
    vsg::ref_ptr<vsg::uintArray> getIndexes(){return indexes;}
    
  private:
    TopoDS_Face face;
    vsg::ref_ptr<vsg::vec3Array> vertices;
    vsg::ref_ptr<vsg::vec3Array> normals;
    vsg::ref_ptr<vsg::vec2Array> coordinates; //texture
    vsg::ref_ptr<vsg::uintArray> indexes;
  };
  
  struct EdgeConvert : public BaseConvert
  {
    EdgeConvert() = delete;
    explicit EdgeConvert(const TopoDS_Edge &eIn, TopoDS_Face fIn = TopoDS_Face())
    : BaseConvert(), edge(eIn), face(fIn) {}
    
    bool generate(); //return false when errors. warnings are still true;
    vsg::ref_ptr<vsg::VertexDraw> buildDraw();
    vsg::ref_ptr<vsg::vec3Array> getVertices(){return vertices;}
    
  private:
    TopoDS_Edge edge;
    TopoDS_Face face;
    vsg::ref_ptr<vsg::vec3Array> vertices;
  };
}

#endif //OCV_CONVERT_H
